package com.example.webmethodsanalyze;

public class PackageStructureException extends Exception {

	public PackageStructureException() {
        super();
    }


    public PackageStructureException(String message) {
        super(message);
    }
}
